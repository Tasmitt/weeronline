package com.compamy;

import org.apache.maven.execution.ExecutionListener;
import org.junit.runner.JUnitCore;

public class TestRunner {
    public static void main(String[] args) {
        JUnitCore runner = new JUnitCore();

        runner.addListener(new com.compamy.ExecutionListener());
        runner.run(TestScript.class, SimpleTestScript.class);
    }
}
