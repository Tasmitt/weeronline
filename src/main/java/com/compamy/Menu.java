package com.compamy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by tasmitt on 25-Jul-17.
 */
public class Menu {

    private WebDriver webDriver;
    private WebDriverWait wait;
    private WebDriverWait wait2;


    public Menu(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 60);
        wait2 = new WebDriverWait(webDriver, 3);
    }

    public void menuOpener(String menuName, String subMenuName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));


        WebElement menuElement;
        WebElement listElement;

        List<WebElement> al = webDriver.findElement(By.cssSelector("ul[class*='nav']")).findElements(By.cssSelector("li[class*='nav']"));

        al = excludeUselessElements(al);

        menuElement = mainMenuItem(al, menuName);
        menuElement.click();

        listElement = findListElement(menuElement,subMenuName);

        click(menuElement,listElement,subMenuName);  //I know it's weird

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));
    }

    public void search(String city, String country){
        WebElement searchField = webDriver.findElement(By.cssSelector("input[class*='search']"));

        searchField.click();
        searchField.sendKeys(city);


        List <WebElement> results;
        WebElement resultContainer;
        List <WebElement> mainSuggestions;
        WebElement mainResult ;

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));
        try{
            wait2.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class=search-result-container]")));
            resultContainer = webDriver.findElement(By.cssSelector("ul[class*=ui-autocomplete]"));
            results = resultContainer.findElements(By.cssSelector("div"));
        }
        catch (Exception ex){
            resultContainer = webDriver.findElement(By.id("search-subContainer"));
            results = resultContainer.findElements(By.cssSelector("div"));
        }

        mainSuggestions = getCitySuggestions(results,city);

        mainResult = getMainSuggestion(mainSuggestions, country);

        mainResult.click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));
    }


    private List<WebElement> excludeUselessElements(List<WebElement> al) {
        if (al.size() == 5) {
            for (int i = 4; i < 0; i--) {
                try {
                    al.get(i).findElement(By.cssSelector("a"));
                } catch (Exception ex) {
                    al.remove(al.get(i));
                    break;
                }
            }
        }
        return al;
    }

    private WebElement mainMenuItem(List<WebElement> al, String menuName){
        WebElement menuElement = null;
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));
        for (int i = 0; i < al.size(); i++) {
            if (al.get(i).findElement(By.cssSelector("a")).getText().equals(menuName)) {
                menuElement = al.get(i);
                break;
            }
        }
        return menuElement;
    }

    private WebElement findListElement(WebElement menuElement, String subMenuName){
        WebElement listElement = null;
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));

        List<WebElement> ml = menuElement.findElements(By.cssSelector("li"));
        if (menuElement.findElement(By.cssSelector("a")).getText().equals("Mijn Plaatsen")) {
            for (int i = 0; i < ml.size(); i++) {
                try {
                    ml.get(i).findElement(By.cssSelector("a")).getText().equals(subMenuName);
                    listElement = ml.get(i);
                }
                catch (Exception ex ){ml.get(i).findElement(By.cssSelector("span")).getText().equals(subMenuName);
                    listElement = ml.get(i);
            }
        }
        }else {
            for (int i = 0; i < ml.size(); i++) {
                if (ml.get(i).findElement(By.cssSelector("a")).getText().equals(subMenuName)) {
                    listElement = ml.get(i);
                    break;
                }
            }
        }
        return listElement;
    }

    private List<WebElement> getCitySuggestions(List<WebElement> results, String city){
        List <WebElement> mainSuggestions = new ArrayList();
        for (int i=0; i < results.size(); i++){
            List<WebElement> li = results.get(i).findElements(By.cssSelector("span"));
            for (int j=0; j < li.size(); j++){
                if (li.get(j).getText().equals(city)){
                    mainSuggestions.add(results.get(i));
                }
            }
        }
        return mainSuggestions;
    }

    private WebElement getMainSuggestion (List<WebElement> mainSuggestions, String country){
        WebElement mainResult = null;
        for (int i=0; i < mainSuggestions.size(); i++){
            List<WebElement> li = mainSuggestions.get(i).findElements(By.cssSelector("span"));
            for (int j=0; j < li.size(); j++){
                if (li.get(j).getText().equals(country)){
                    mainResult = mainSuggestions.get(i);
                }
            }
        }
        return mainResult;
    }

    private void click(WebElement menuElement,WebElement listElement, String subMenuName){try{
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));
        listElement.click();
    } catch (NullPointerException ex){
        if (subMenuName.equals("Zonkracht")) {
            try {
                listElement = findListElement(menuElement, "UV-index");
                listElement.click();
            } catch (NoSuchElementException ex4) {
  //              throw new NoSuchElementException();
            }
        }
        else if (subMenuName.equals("UV-index")){
            try {
                listElement = findListElement(menuElement, "Zonkracht");
                listElement.click();
            }catch (Exception ex1){
                throw new NoSuchElementException();
            }
        }
        else if (subMenuName.equals("Rokjes weer")) {
            try {
                listElement = findListElement(menuElement, "Rokjes");
                listElement.click();
            } catch (Exception ex2) {
                throw new NoSuchElementException();
            }
        }
        else if (subMenuName.equals("Rokjes")) {
            try {
                listElement = findListElement(menuElement, "Rokjes weer");
                listElement.click();
            }catch (Exception ex3){
                throw new NoSuchElementException();
            }
        }
    }
    }

}