package com.compamy;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by tasmitt on 25-Jul-17.
 */
public class SimpleTestScript {

        private WebDriver webDriver = new ChromeDriver();
        private WebDriverWait wait = new WebDriverWait(webDriver, 15);

        @Before
        public void setUp(){
            webDriver.get("http://www.weeronline.nl/");
        }

    //page to page navigarion
        @Test
        public void SimpleTest(){
            By navBar = By.cssSelector("div[class*='nav']");

            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".container.mainContainer")));

            //close cookie page
            webDriver.findElement(By.className("btn-accept")).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(navBar));

            //go from homepage to Actueel: Maand- en seizoensoverzichten
            webDriver.findElement(By.id("actueel")).click();
            webDriver.findElement(By.id("actueel-li-6")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(navBar));

            String homeCssSelector = "home-container-module__fixedContainerWidth___38ohA";
            String message2 = "Link is not correct.";

            Assert.assertTrue(message2, webDriver.getCurrentUrl().equals("http://nieuws.weeronline.nl/overzichten/"));

            //go from Maand- en seizoensoverzichten to Wereldweer: Europa
            WebElement menuItem1 = webDriver.findElement(By.id("menu-item-1"));
            menuItem1.click();

            WebElement menuSubItem1 = fetchItem(menuItem1,"Europa");

            menuSubItem1.click();

            Assert.assertTrue(message2, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/4"));

            //go from Europa to Weercijfers: fietsen
            WebElement menuItem2 = webDriver.findElement(By.id("menu-item-2"));
            menuItem2.click();

            WebElement menuSubItem2 = fetchItem(menuItem2,"Fietsen");

            menuSubItem2.click();

            Assert.assertTrue(message2, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Fietsen-Nederland/135/5"));

        }

        @After
        public void cleanUp(){
            webDriver.quit();
        }

        public WebElement fetchItem(WebElement menuItem, String menuSubItemName){

            WebElement menuSubItem = null;
            List<WebElement> subI = menuItem.findElements(By.cssSelector("li"));
            for (int i =0; i < subI.size(); i++){
                if (subI.get(i).findElement(By.cssSelector("a")).getText().equals(menuSubItemName)) {
                    menuSubItem = subI.get(i);
                    break;
                }
            }
            return menuSubItem;
        }

}
