package com.compamy;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by tasmitt on 18-Jul-17.
 */
public class TestScript {
    private WebDriver webDriver = new ChromeDriver();
    private WebDriverWait wait = new WebDriverWait(webDriver, 15);
    private Menu menu = new Menu(webDriver);


    @Before
    public void setUp(){
        webDriver.get("http://www.weeronline.nl/");

    }

    //page tp page navigation+search
    @Test
    public void Test() throws InterruptedException {
        String message = "Wrong link was opened";


        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".container.mainContainer")));
        webDriver.findElement(By.className("btn-accept")).click();


        menu.menuOpener("Actueel", "Zonkracht");
        Assert.assertTrue(message, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Zonkracht-Nederland/135"));

        menu.menuOpener("Wereldweer","Europa");
        Assert.assertTrue(message, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/4"));

        menu.menuOpener("Actueel", "Zonkracht");
        Assert.assertTrue(message, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Zonkracht-Nederland/135"));

        menu.menuOpener("Mijn Plaatsen", "Amsterdam");
        Assert.assertTrue(message, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Nederland/Amsterdam/4058223"));

        menu.menuOpener("Weercijfers", "Rokjes");
        Assert.assertTrue(message, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Rokjes-Nederland/135/9"));

        menu.menuOpener("Weercijfers", "Golf");
        Assert.assertTrue(message, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Golf-Nederland/135/6"));

        menu.menuOpener("Actueel", "Zonkracht");
        Assert.assertTrue(message, webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Zonkracht-Nederland/135"));

        menu.search("Haarlem", "Nederland");

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul[class*='nav']")));

        menu.search("Amsterdam", "Nederland");
        Assert.assertTrue(webDriver.getCurrentUrl().equals("http://www.weeronline.nl/Europa/Nederland/Amsterdam/4058223"));

    }


    @After
    public void cleanUp(){
        webDriver.quit();
    }



}
